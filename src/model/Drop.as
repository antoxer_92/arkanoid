package model 
{
	import Box2D.Collision.Shapes.b2CircleShape;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2BodyDef;
	import Box2D.Dynamics.b2FilterData;
	import Box2D.Dynamics.b2FixtureDef;
	import Box2D.Dynamics.b2World;
	import flash.display.DisplayObject;
	import flash.filters.GlowFilter;
	/**
	 * ...
	 * @author Antoxer
	 */
	public class Drop extends Body 
	{
		private var _texture:DisplayObject;
		private var _type:uint = 0;
		
		public function Drop(world:b2World, type:uint, x:Number, y:Number) 
		{
			super(world);
			this.type = type;
			setPosition(x, y);
			initBody(x, y);
		}
		
		private function initBody(x:Number, y:Number):void 
		{
			var fixtureDef:b2FixtureDef = new b2FixtureDef();
			var filter:b2FilterData = new b2FilterData();
			var shape:b2CircleShape = new b2CircleShape(B2Transform.pixelsToMeters(Math.round(_texture.width / 2)));
			var bodyDef:b2BodyDef = new b2BodyDef();
			
			filter.categoryBits = 0x0004;
			filter.maskBits = 0x0002;
			
			fixtureDef.density = 1;
			fixtureDef.friction = 0;
			fixtureDef.restitution = 1;
			fixtureDef.shape = shape;
			fixtureDef.filter = filter;

			bodyDef.type = b2Body.b2_dynamicBody;
			bodyDef.fixedRotation = true;
			bodyDef.allowSleep = false;
			bodyDef.position.Set(B2Transform.pixelsToMeters(x), B2Transform.pixelsToMeters(y));

			create(bodyDef, fixtureDef, {type:'drop', hitted:false, value:_type});
			
			body.ApplyForce(new b2Vec2(0, 9.8), new b2Vec2(B2Transform.pixelsToMeters(x), B2Transform.pixelsToMeters(y)));
		}
		
		override public function setPosition(x:Number, y:Number):void 
		{
			this.x = x;
			this.y = y;
			super.setPosition(x, y);
		}
		
		public function get type():uint 
		{
			return _type;
		}
		
		public function set type(value:uint):void 
		{
			if (_type == value) return;
			_type = value;
			
			if (_texture) removeChild(_texture);

			_texture = Library['drop' + String(type)];
			_texture.x = _texture.y = -Math.round(_texture.width/2);
			addChild(_texture);
		}
		
	}

}