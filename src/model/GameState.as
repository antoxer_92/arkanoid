package model
{
	/**
	 * ...
	 * @author Antoxer
	 */
	public class GameState
	{
		public static const MAIN_MENU:uint = 0;
		static public const FINISH_MENU:uint = 2;
		static public const LEVEL_IS_LOADING:uint = 6;
		static public const LEVEL_LOADED:uint = 8;
		static public const LEVEL_IS_READY:uint = 3;
		static public const LEVEL_IS_PLAYING:uint = 4;
		static public const LEVEL_PAUSED:uint = 7;
		static public const LEVEL_COMPLETED:uint = 5;
		static public const WINNER_MENU:uint = 9;
	}
}