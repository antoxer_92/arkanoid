package model
{
	import Box2D.Collision.Shapes.b2CircleShape;
	import Box2D.Common.Math.b2Math;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2BodyDef;
	import Box2D.Dynamics.b2FilterData;
	import Box2D.Dynamics.b2FixtureDef;
	import Box2D.Dynamics.b2World;
	import com.greensock.TweenLite;
	import com.greensock.TweenMax;
	import com.greensock.plugins.GlowFilterPlugin;
	import com.greensock.plugins.TweenPlugin;
	import flash.display.DisplayObject;
	import flash.display.GradientType;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.filters.BlurFilter;
	import flash.filters.GlowFilter;
	import flash.geom.Matrix;
	
	/**
	 * ...
	 * @author Antoxer
	 */
	public class Ball extends Body
	{
		private static const MAX_RADIUS:Number = 25;
		
		private var _texture:DisplayObject;
		private var _radius:Number = 0;
		private var _speed:Number;
		private var _tail:Shape = new Shape();
		private var _damage:uint = 1;
		private var _tweens:Object = {};
		
		public function Ball(world:b2World, x:Number, y:Number, r:Number):void
		{
			super(world);
			
			_tail = new Shape();
			_tail.alpha = 0.5
			addChild(_tail);
			_texture = Library.ball;
			_texture.filters = [new GlowFilter(0xFC6703, 0.7, 8, 8)];
			addChild(_texture);
			radius = r;
			setPosition(x, y);
			initBody(x, y, r);
		}
		
		private function initBody(x:Number, y:Number, r:Number):void 
		{
			var fixtureDef:b2FixtureDef = new b2FixtureDef();
			var filter:b2FilterData = new b2FilterData();
			var shape:b2CircleShape = new b2CircleShape(B2Transform.pixelsToMeters(r));
			var bodyDef:b2BodyDef = new b2BodyDef();
			
			filter.categoryBits = 0x0002;
			filter.maskBits = 0x0002;

			fixtureDef.density = 1;
			fixtureDef.friction = 0;
			fixtureDef.restitution = 1;
			fixtureDef.shape = shape;
			fixtureDef.filter = filter;

			bodyDef.type = b2Body.b2_dynamicBody;
			bodyDef.fixedRotation = true;
			bodyDef.bullet = true;
			bodyDef.allowSleep = false;
			bodyDef.position.Set(B2Transform.pixelsToMeters(x), B2Transform.pixelsToMeters(y));
			
			create(bodyDef, fixtureDef, {type:'ball', isOut:false});
		}
		
		public function draw(r:Number):void
		{
			_texture.width = _texture.height = r * 2;
			_texture.x = _texture.y = -r;
		}
		
		override public function set width(value:Number):void
		{
			return;
		}
		
		override public function set height(value:Number):void
		{
			return;
		}
		
		public function get radius():Number 
		{
			return _radius;
		}
		
		public function set radius(value:Number):void 
		{
			_radius = (value > MAX_RADIUS) ? MAX_RADIUS : value;
			draw(_radius);
			setSize(_radius, _radius);
		}
		
		override public function setPosition(x:Number, y:Number):void 
		{
			this.x = x;
			this.y = y;
			super.setPosition(x, y);
		}
		
		public function setRadiusSmoothly(value:Number):void 
		{
			if (_radius == value) return;
			
			var sizeObj:Object = {radius:_radius};
			
			_radius = (value > MAX_RADIUS) ? MAX_RADIUS : value;
			setSize(_radius, _radius);
			filters = null;
			_tweens.radius = TweenLite.to(sizeObj, 1, {radius:_radius, onUpdateParams:[sizeObj], onUpdate:onUpdateSize, overwrite:true});
			_tweens.radiusGrad = TweenMax.to(this, 0.5, {glowFilter:{color:0x0080FF, blurX:8, blurY:8, strength:1, quality:3, alpha:1}, yoyo:true, repeat:3, overwrite:true});
		}
		
		private function onUpdateSize(obj:Object):void 
		{
			draw(obj.radius);
		}
		
		override public function increaseLinearVelocity(mx:Number, my:Number, bx:Number = 0, by:Number = 0):void
		{
			super.increaseLinearVelocity(mx, my, bx, by);
			filters = null;
			_tweens.allGrad = TweenMax.to(this, 0.5, {glowFilter:{color:(mx < 1) ? 0x00FF00 : 0xFF8000, blurX:8, blurY:8, strength:1, quality:3, alpha:1}, yoyo:true, repeat:3, overwrite:true});
		}
		
		/*public function drawTail(radius:Number, smothly:Boolean = true):void 
		{
			if (body == null) return;
			
			var v:b2Vec2 = body.GetLinearVelocity();
			var vx:Number = B2Transform.metersToPixels(v.x);
			var vy:Number = B2Transform.metersToPixels(v.y);
			var angle:Number = Math.atan2(vy, vx) * 180 / Math.PI + 90;
			var w:Number = Math.round(radius * 0.5);
			var by:Number = Math.round(radius * 0.7);
			var h:Number = Math.round(Math.sqrt(vx*vx + vy*vy)/7) + by;

			_tail.graphics.clear();
			_tail.graphics.beginFill(0xFFBA75, 0.7);
			_tail.graphics.moveTo( -w, by);
			_tail.graphics.lineTo(w,by);
			_tail.graphics.lineTo(0, h);
			_tail.graphics.lineTo(-w,by);
			_tail.graphics.endFill();
			_tail.rotationZ = angle;
			_tail.filters = [new GlowFilter(0xFC6703, 1, 8, 8, 5, 2)];
			
			if (!smothly) return;
			
			_tail.scaleX = 0.1;
			_tail.scaleY = 0.1;
			_tweens.tail = TweenLite.to(_tail, 1, {scaleX:1, scaleY:1, overwrite:true});
		}*/
		
		public function clearEffects():void 
		{
			for (var name:String in _tweens) 
			{
				if (_tweens[name])
				{
					_tweens[name].kill();
				}
			}
			
			filters = null;
		}
		
		public function moveSmothly():void 
		{
			var tail:Shape = new Shape();
			tail.graphics.beginFill(0xFC6703, 0.7);
			tail.graphics.drawEllipse( -_radius, - radius, radius * 2, radius * 2);
			tail.graphics.endFill();
			tail.x = x;
			tail.y = y;
			if (parent) parent.addChildAt(tail, parent.getChildIndex(this));
			TweenLite.to(tail, 0.5, {alpha:0.1, scaleX:0.1, scaleY:0.1, blurFilter:{blurX:10,blurY:10}, onCompleteParams:[tail], onComplete:removeTail});
			move();
		}
		
		private function removeTail(obj:Object):void 
		{
			var tail:Shape = obj as Shape;
			if (tail.parent) tail.parent.removeChild(tail);
		}
		
		public function get speed():Number 
		{
			return _speed;
		}
		
		public function set speed(value:Number):void 
		{
			_speed = value;
		}
		
		public function get damage():uint 
		{
			return _damage;
		}
		
		public function set damage(value:uint):void 
		{
			_damage = value > 5 ? 5 : value;
		}
	}
	
}