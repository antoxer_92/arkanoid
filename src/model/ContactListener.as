package model 
{
	import Box2D.Collision.Shapes.b2Shape;
	import Box2D.Collision.b2ContactPoint;
	import Box2D.Dynamics.Contacts.b2Contact;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2ContactListener;
	
	/**
	 * ...
	 * @author Antoxer
	 */
	public class ContactListener extends b2ContactListener 
	{
		
		public function ContactListener() 
		{
			
		}
		
		//override public function Add(point:b2ContactPoint):void
		override public function BeginContact(point:b2Contact):void
		{
			var body1:b2Body = point.GetFixtureA().GetBody();
			var body2:b2Body = point.GetFixtureB().GetBody();
			var data1:Object = body1.GetUserData() as Object;
			var data2:Object = body2.GetUserData() as Object;
			var type1:String = data1 ? data1.type as String : null;
			var type2:String = data2 ? data2.type as String : null;

			if (type1 && type2)
			{
				if ((type1 == 'ball') && (type2 == 'bottomWall')) ballOut(body1);
				else if ((type2 == 'ball') && (type1 == 'bottomWall')) ballOut(body2);
				
				if ((type1 == 'ball') && (type2 == 'topWall')) wallHitted(body2);
				else if ((type2 == 'ball') && (type1 == 'topWall')) wallHitted(body1);
				
				if ((type1 == 'ball') && (type2 == 'leftWall')) wallHitted(body2);
				else if ((type2 == 'ball') && (type1 == 'leftWall')) wallHitted(body1);
				
				if ((type1 == 'ball') && (type2 == 'rightWall')) wallHitted(body2);
				else if ((type2 == 'ball') && (type1 == 'rightWall')) wallHitted(body1);
				
				else if ((type1 == 'ball') && (type2 == 'board')) boardHitted(body2);
				else if ((type2 == 'ball') && (type1 == 'board')) boardHitted(body1);
		
				else if ((type1 == 'ball') && (type2 == 'brick')) brickHitted(body2);
				else if ((type2 == 'ball') && (type1 == 'brick')) brickHitted(body1);
				
				else if ((type1 == 'drop') && (type2 == 'board')) dropHitted(body1, true);
				else if ((type2 == 'drop') && (type1 == 'board')) dropHitted(body2, true);
				
				else if ((type1 == 'drop') && (type2 == 'bottomWall')) dropHitted(body1, false);
				else if ((type2 == 'drop') && (type1 == 'bottomWall')) dropHitted(body2, false);
			}

			//super.Add(point);
			super.BeginContact(point);
		}
		
		private function dropHitted(body:b2Body, hittedType:Boolean):void 
		{
			var data:Object = body.GetUserData() as Object;
			data.hitted = true;
			data.hittedType = hittedType;
		}
		
		private function wallHitted(body:b2Body):void 
		{
			var data:Object = body.GetUserData() as Object;
			data.hitted = true;
		}
		
		private function boardHitted(body:b2Body):void 
		{
			var data:Object = body.GetUserData() as Object;
			data.hitted = true;
		}
		
		private function brickHitted(body:b2Body):void 
		{
			var data:Object = body.GetUserData() as Object;
			data.hitted = true;
		}
		
		private function ballOut(body:b2Body):void 
		{
			var data:Object = body.GetUserData() as Object;
			data.isOut = true;
		}
		
	}

}