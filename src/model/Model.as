package model
{
	import Box2D.Collision.Shapes.b2PolygonShape;
	import Box2D.Collision.Shapes.b2Shape;
	import Box2D.Collision.Shapes.b2EdgeShape;
	import Box2D.Collision.b2AABB;
	import Box2D.Common.Math.b2Math;
	import Box2D.Common.Math.b2Transform;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2BodyDef;
	import Box2D.Dynamics.b2ContactListener;
	import Box2D.Dynamics.b2DebugDraw;
	import Box2D.Dynamics.b2FilterData;
	import Box2D.Dynamics.b2FixtureDef;
	import Box2D.Dynamics.b2World;
	import base.BaseClass;
	import controller.MoveDirection;
	import events.GameEvent;
	import events.GlobalDispatcher;
	import flash.display.Shape;
	import flash.events.Event;
	import flash.geom.Rectangle;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import ru.axr.buttons.events.ButtonEvent;
	import view.SoundManager;

	public class Model extends BaseClass
	{
		private const numLevels:uint = 5;
		private const numLives:uint = 5;
		public static const gameArea:Rectangle = new Rectangle(10, 10, 800, 780);
		public static const b2Width:Number = gameArea.left + gameArea.right;
		public static const b2Height:Number = gameArea.top + gameArea.bottom;
		
		//Значения параметров по умолчанию
		private static const boardSpeed:Number = 10;
		private static const boardWidth:Number = 100;
		private static const boardHeight:Number = 10;
		private static const ballSpeed:Number = 6;
		private static const ballRadius:Number = 10;
		private static const brickWidth:Number = 25;
		private static const brickHeight:Number = 15;

		//Данные по игре
		private var _state:uint;
		private var _level:uint;
		private var _score:uint;
		private var _lives:uint;
		private var _bonus:Number;
		
		//Игровые объекты и физическте тела
		private var _board:Board;
		private var _ball:Ball;
		private var _bricks:Vector.<Brick>;
		private var _drops:Vector.<Drop>;
		
		private var _world:b2World;
		private var _topWallBody:b2Body;
		private var _leftWallBody:b2Body;
		private var _rightWallBody:b2Body;
		
		public function Model()
		{
			super();
		}

		override protected function onDestroy():void
		{
			
		}
		
		override protected function onCreate():void
		{
			_level = 0;
			_bricks = new Vector.<Brick>;
			_drops = new Vector.<Drop>;
			createWorld();
			createWalls();
			
			setState(GameState.MAIN_MENU);
		}
		
		private function setState(value:int):void 
		{
			_state = value;
			GlobalDispatcher.dispatchEvent(new GameEvent(GameEvent.STATE_CHANGED, {state:_state}));
		}
		
		private function createWorld():void 
		{
			var gravity:b2Vec2 = new b2Vec2(0, 0);
			
			_world = new b2World(gravity, true);
			_world.SetContactListener(new ContactListener());
		}
		
		private function createWalls():void 
		{
			_topWallBody = createWall(b2Width / 2, gameArea.top / 2, b2Width / 2, gameArea.top / 2, 0x0002, 0x0002, {type:'topWall', hitted:false});
			_leftWallBody = createWall(gameArea.left / 2, b2Height / 2, gameArea.left / 2, b2Height / 2, 0x0002, 0x0002, {type:'leftWall', hitted:false});
			createWall(b2Width / 2, gameArea.bottom + gameArea.top / 2, b2Width / 2, gameArea.top / 2, 0x0002, 0x0002 | 0x0004, {type:'bottomWall', hitted:false});
			_rightWallBody = createWall(gameArea.right + gameArea.left / 2, b2Height / 2, gameArea.left / 2, b2Height / 2, 0x0002, 0x0002, {type:'rightWall', hitted:false});
		}

		private function createWall(x:Number, y:Number, w:Number, h:Number, categoryBits:uint, maskBits:uint, userData:Object):b2Body
		{
			var fixtureDef:b2FixtureDef = new b2FixtureDef();
			var filter:b2FilterData = new b2FilterData();
			var shape:b2Shape = b2PolygonShape.AsBox(B2Transform.pixelsToMeters(w), B2Transform.pixelsToMeters(h));
			var bodyDef:b2BodyDef = new b2BodyDef();
			var body:b2Body;
			
			filter.categoryBits = categoryBits;
			filter.maskBits = maskBits;

			fixtureDef.density = 0;
			fixtureDef.restitution = 1;
			fixtureDef.shape = shape;
			fixtureDef.filter = filter;

			bodyDef.type = b2Body.b2_staticBody;
			bodyDef.position.Set(B2Transform.pixelsToMeters(x), B2Transform.pixelsToMeters(y));
			
			body = _world.CreateBody(bodyDef);
			body.CreateFixture(fixtureDef);
			body.SetUserData(userData);
			
			return body;
		}

		private function createBoard(x:Number, y:Number, w:Number, h:Number):void
		{
			_board = new Board(_world, x, y, w, h);
			Facade.view.addGameChild(_board);
		}
		
		private function createBall(x:Number, y:Number, r:Number):void
		{
			_ball = new Ball(_world, x, y , r);
			Facade.view.addGameChild(_ball);
		}
		
		private function createBrick(type:uint, x:Number, y:Number):void 
		{
			_bricks.push(new Brick(_world, type, getRandomDrop(), x, y, brickWidth, brickHeight));
			//Facade.view.addGameChild(_bricks[_bricks.length-1]);
		}
		
		private function getRandomDrop():uint 
		{
			var rand:Number = uint(Math.random() * 20);
			var drop:uint = (rand > 5) ? 0 : rand;
			return drop;		
		}
		
		private function createDrop(type:uint, x:Number, y:Number):void
		{
			_drops.push(new Drop(_world, type, x, y));
			Facade.view.addGameChild(_drops[_drops.length - 1]);
		}

		//Начинаем уровень, запустив шар
		public function startLevel():void 
		{
			setState(GameState.LEVEL_IS_PLAYING);
			repulseBall();
		}
		
		//Возобновляем игру
		public function resumeLevel():void 
		{
			Facade.stage.addEventListener(Event.ENTER_FRAME, enterFrameHandler);
			setState(GameState.LEVEL_IS_PLAYING);
		}
		
		//Приостанавливаем игру
		public function pauseLevel():void 
		{
			Facade.stage.removeEventListener(Event.ENTER_FRAME, enterFrameHandler);
			setState(GameState.LEVEL_PAUSED);
		}
		
		//Очищаем уровень, удаляя игровые объекты
		private function clearLevel():void
		{
			var i:int = 0;
			
			for (i = 0; i < _bricks.length; i++) _bricks[i].destroy();
			for (i = 0; i < _drops.length; i++) _drops[i].destroy();
			
			_bricks.length = 0;
			_drops.length = 0;
			_board.destroy();
			_ball.destroy();
			
			if (_state == GameState.LEVEL_IS_PLAYING) Facade.stage.removeEventListener(Event.ENTER_FRAME, enterFrameHandler);
		}
		
		//Загружаем уровень из внешнего источника
		public function loadLevel():void
		{
			var loader:URLLoader;
			var request:URLRequest;
			
			level++;
			
			//Если уровень первый, то обнулим данные
			if (_level == 1)
			{
				score = 0;
				lives = numLives;
			}
			
			setState(GameState.LEVEL_IS_LOADING);
			
			loader = new URLLoader();
			request = new URLRequest('levels/' + String(_level) + '.txt');
			request.method = URLRequestMethod.GET;
			loader.dataFormat = URLLoaderDataFormat.TEXT;
			loader.addEventListener(Event.COMPLETE, onLevelLoaded);
			loader.load(request);
		}
		
		//Данные уровня занружены
		private function onLevelLoaded(e:Event):void 
		{
			var loader:URLLoader = e.currentTarget as URLLoader;
			var data:Object = JSON.parse(loader.data as String).level;
			
			loader.removeEventListener(e.type, onLevelLoaded);	
			initLevel(data);
		}
		
		//Инициальзируем загруженные данные уровня
		private function initLevel(data:Object):void
		{
			var bricksData:Array = data.bricks as Array;
			var bw:Number = boardWidth-level*10
			
			createBoard(400, 700, bw < 20 ? 20 : bw, boardHeight);
			createBall(400, 500, ballRadius);
			_board.defaultSpeed = boardSpeed - Number(_level);
			_ball.speed = ballSpeed + Number(_level);

			//Создаем элементы, i - номер строки, j - номер столбца
			for (var i:int = 0; i < bricksData.length; i++) 
			{
				var row:Array = bricksData[i] as Array;
				
				for (var j:int = 0; j < row.length; j++) 
				{
					var brickType:uint = row[j] as uint;
					if ((brickType == BrickType.EMPTY) || (gameArea.left + j*brickWidth + brickWidth > gameArea.right)) continue;
					
					createBrick(brickType, gameArea.left + (1 + j*2)*brickWidth, gameArea.top + (1 + i*2)*brickHeight);
				}
			}

			setState(GameState.LEVEL_LOADED);
			resetLevel();
			Facade.stage.addEventListener(Event.ENTER_FRAME, enterFrameHandler);
		}
		
		
		/*Управление игровыми объектами*/

		//Задаем направление движения доски
		public function setBoardDirection(value:int):void 
		{
			if (_board.direction == value) return;
			_board.direction = value;
			if (state == GameState.LEVEL_IS_PLAYING) _board.body.SetLinearVelocity(new b2Vec2(B2Transform.pixelsToMeters(_board.direction * _board.speed * fps), 0));
		}
		
		//Толкаем шар в указанном направлении
		private function repulseBall():void 
		{
			//body.ApplyImpulse(new b2Vec2(Math.cos(Math.PI)*b2Transform.pixelsToMeters(_ball.defaultSpeed)*10, Math.sin(-Math.PI/2)*b2Transform.pixelsToMeters(_ball.defaultSpeed)*10), new b2Vec2(body.GetWorldCenter().x, body.GetWorldCenter().y));
			_ball.setLinearVelocity(Math.cos(Math.PI) * _ball.speed, Math.sin( -Math.PI / 2) * _ball.speed);
			//_ball.drawTail(_ball.radius);
		}

		//Здесь следим за физикой box2d
		//Данный обработчик активируется лишь только с момента, когда state = LEVEL_IS_READY
		//Данный обработчик деактивируется при проигрыше
		//Значение state = LEVEL_PAUSED останавливает выполнение данного обработчика
		//При возобновлении значения состояния state = LEVEL_IS_PLAYING возобновляется выполнение данного обработчика
		private function enterFrameHandler(e:Event):void 
		{
			var data:Object;
			var i:int = 0;

			//Если шар еще не запущен, то перемещаем доску
			if (setBeginPosition()) return;
			
			//Просчет физики
			_world.Step(1 / 60, 10, 10);
			//_world.ClearForces();
			//Если шар запущен, то котролируем выход борда за стены
			controlBoard();
			
			//Перемещаем объекты согласно их телам
			_ball.moveSmothly();
			_board.moveSmothly();
			for (i = 0; i < _drops.length; i++) 
			{
				data = _drops[i].body.GetUserData();
				if (data.hitted) dropHit(i, data);
				else _drops[i].move();
			}
			
			//Шар столкнулся с верхней стеной
			data = _topWallBody.GetUserData();
			if (data.hitted) wallHit(data);
			
			//Шар столкнулся с левой стеной
			data = _leftWallBody.GetUserData()
			if (data.hitted) wallHit(data);
			
			//Шар столкнулся с правой стеной
			data = _rightWallBody.GetUserData()
			if (data.hitted) wallHit(data);
			
			//Шар столкнулся с бордом
			data = _board.body.GetUserData()
			if (data.hitted) boardHit(data);

			//Шар улетел за доску
			data = _ball.body.GetUserData()
			if (data.isOut) ballOut(data);
			
			//Отслеживаем столконовение шара с кирпичиками
			for (i = 0; i < _bricks.length; i++) 
			{
				data = _bricks[i].body.GetUserData();
				
				if (data.hitted)
				{
					ballHit(i, data);
					break;
				}
			}
		}
		
		//Если шар еще не запущен, передвигаем борд с шаром
		private function setBeginPosition():Boolean 
		{
			if (_state != GameState.LEVEL_IS_READY) return false;
			
			var boardX:Number;
			
			if (_board.direction == MoveDirection.LEFT) 
			{
				boardX = _board.x - _board.speed;
				_board.setPosition((boardX - _board.width < gameArea.left) ? gameArea.left + _board.width : boardX, _board.y);
			}
			else if (_board.direction == MoveDirection.RIGHT) 
			{
				boardX = _board.x + _board.speed;
				_board.setPosition((boardX + _board.width > gameArea.right) ? gameArea.right - _board.width : boardX, _board.y);
			}
				
			_ball.setPosition(_board.x, _board.y - _board.height - _ball.radius);
			
			return true;
		}
		
		//Если шар запущен контролируем выход борда за стены
		private function controlBoard():void 
		{
			if (_state != GameState.LEVEL_IS_PLAYING) return;
			
			_board.controlPosition(_leftWallBody.GetPosition().x + B2Transform.pixelsToMeters(gameArea.left / 2), _rightWallBody.GetPosition().x - B2Transform.pixelsToMeters(gameArea.left / 2));
		}
		
		//Дроп попал в борд или в нижнюю стену
		private function dropHit(i:int, data:Object):void 
		{
			if (data.hittedType) applyDrop(data.value);

			_drops[i].destroy();
			_drops.removeAt(i);
		}
		
		//Применяет бонус дропа
		private function applyDrop(value:uint):void 
		{
			switch (value) 
			{
				case 1:
					_board.setSizeSmoothly(Math.round(_board.width + 20), _board.height);
				break;
				case 2:
					_board.setSpeedSmoothly(Math.round(_board.speed*1.5));
				break;
				case 3:
					_ball.setRadiusSmoothly(Math.round(_ball.radius + 5));
					_ball.damage++;
				break;
				case 4:
					_ball.increaseLinearVelocity(1.5, 1.5);
				break;
				case 5:
					_ball.increaseLinearVelocity(1/1.5, 1/1.5);
				break;
			}
			
			GlobalDispatcher.dispatchEvent(new GameEvent(GameEvent.PLAY_SOUND, {name:SoundManager.BONUS}));
		}
		
		//Шар попал в кирпич
		private function ballHit(i:int, data:Object):void 
		{
			var center:b2Vec2;
			var inc:uint;
			var bx:Number;
			var by:Number;
			
			data.hitted = false;
			GlobalDispatcher.dispatchEvent(new GameEvent(GameEvent.PLAY_SOUND, {name:SoundManager.BALL_BRICK}));
			
			_bricks[i].health -= _ball.damage;
			
			if (_bricks[i].health < 1)
			{
				inc = Math.round(_bricks[i].type * 100 * _bonus);
				score += inc;
				_bonus += _bricks[i].type / 10;
				
				center = _bricks[i].body.GetWorldCenter();
				bx = B2Transform.metersToPixels(center.x);
				by = B2Transform.metersToPixels(center.y)
				_bricks[i].destroy();
				_bricks.removeAt(i);
				
				if (data.drop > 0) createDrop(data.drop, bx, by);
				
				Facade.view.showVanishText('+' + String(inc), bx, by);
			}

			if (_bricks.length == 0) levelComplete();
		}
		
		//Шар попал в стену
		private function wallHit(data:Object):void 
		{
			data.hitted = false;
			GlobalDispatcher.dispatchEvent(new GameEvent(GameEvent.PLAY_SOUND, {name:SoundManager.BALL_WALL}));
		}
		
		//Шар попал в борд
		private function boardHit(data:Object):void 
		{
			data.hitted = false;
			_bonus = 1;
			
			if (_state == GameState.LEVEL_IS_PLAYING) GlobalDispatcher.dispatchEvent(new GameEvent(GameEvent.PLAY_SOUND, {name:SoundManager.BALL_BOARD}));
		}
		
		//Шар улетел за борд
		private function ballOut(data:Object):void 
		{
			data.isOut = false;
			GlobalDispatcher.dispatchEvent(new GameEvent(GameEvent.PLAY_SOUND, {name:SoundManager.BALL_OUT}));
			
			if (_lives == 0) gameOver();
			else 
			{
				lives--;
				resetLevel();
			}
		}
		
		//Устанавливаем начальные значения Шару и борду
		private function resetLevel():void 
		{
			_bonus = 1;
			for (var i:int = 0; i < _drops.length; i++) _drops[i].destroy();

			_board.clearEffects();
			_board.setLinearVelocity(0, 0);
			_board.setSize(boardWidth, boardHeight);
			_board.speed = _board.defaultSpeed;
			_board.direction = MoveDirection.NONE;
			_board.setPosition(gameArea.left + Math.round((gameArea.width)/2), gameArea.bottom - _board.height * 4);

			_ball.clearEffects();
			_ball.setLinearVelocity(0, 0);
			_ball.setAngularVelocity(0);
			_ball.radius = ballRadius;
			_ball.damage = 1;
			_ball.setPosition(_board.x, _board.y - _board.height - _ball.radius);
			
			setState(GameState.LEVEL_IS_READY);
		}

		//Уровень пройден
		private function levelComplete():void 
		{
			clearLevel();
			
			//Если это последний уровень, то переходим в меню победителя
			if (_level == numLevels)
			{
				setState(GameState.WINNER_MENU);
			}
			//Иначе переходим к следующему уровню
			else
			{
				GlobalDispatcher.dispatchEvent(new GameEvent(GameEvent.PLAY_SOUND, {name:SoundManager.LEVEL_COMPLETE}));
				loadLevel();
				//setState(GameState.LEVEL_COMPLETED);
			}
		}
		
		//Игра окончена
		private function gameOver():void 
		{
			clearLevel();
			level = 0;
			setState(GameState.FINISH_MENU);
		}
		
		//Переводим игру в состояние инициализации
		public function toInitialState():void 
		{
			clearLevel();
			level = 0;
			setState(GameState.MAIN_MENU);
		}
		
		public function firstLevel():void 
		{
			level = 0;
			loadLevel();
		}
		
		/*Setters & Getters*/
		public function get level():uint 
		{
			return _level;
		}
		
		public function set level(value:uint):void 
		{
			_level = value;
			Facade.view.level = value;
		}
		
		public function get score():uint 
		{
			return _score;
		}
		
		public function set score(value:uint):void 
		{
			_score = value;
			Facade.view.score = value;
		}
		
		public function get lives():uint 
		{
			return _lives;
		}
		
		public function set lives(value:uint):void 
		{
			_lives = value;
			Facade.view.lives = value;
		}
		
		public function get state():uint 
		{
			return _state;
		}
		
		public function get bricks():Vector.<Brick> 
		{
			return _bricks;
		}
		
		private function get fps():Number
		{
			return Facade.stage.frameRate;
		}
	}
}