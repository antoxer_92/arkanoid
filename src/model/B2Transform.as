package model 
{
	/**
	 * ...
	 * @author Antoxer
	 */
	public class B2Transform 
	{
		public static const b2Ratio:Number = 30;
		
		
		public static function pixelsToMeters(pixels:Number):Number
		{
			return pixels / b2Ratio;
		}
		
		public static function metersToPixels(meters:Number):Number
		{
			return meters * b2Ratio;
		}
		
	}

}