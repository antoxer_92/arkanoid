package model
{
	import Box2D.Collision.Shapes.b2PolygonShape;
	import Box2D.Collision.Shapes.b2Shape;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2BodyDef;
	import Box2D.Dynamics.b2FilterData;
	import Box2D.Dynamics.b2FixtureDef;
	import Box2D.Dynamics.b2World;
	import com.greensock.TweenLite;
	import com.greensock.TweenMax;
	import flash.display.GradientType;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.filters.GlowFilter;
	import flash.geom.Matrix;
	
	/**
	 * ...
	 * @author Antoxer
	 */
	public class Board extends Body
	{
		private static const MAX_WIDTH:Number = 250;
		
		private var _speed:Number;
		private var _defaultSpeed:Number;
		private var _width:Number = 100;
		private var _height:Number = 20;
		private var _direction:int;
		private var _shape:Shape;
		private var _tweens:Object = {};
		
		public function Board(world:b2World, x:Number, y:Number, w:Number, h:Number):void
		{
			super(world);
			_shape = new Shape();
			addChild(_shape);
			setSize(w, h);
			setPosition(x, y);
			initBody(x, y, w, h);
		}
		
		private function initBody(x:Number, y:Number, w:Number, h:Number):void 
		{
			var fixtureDef:b2FixtureDef = new b2FixtureDef();
			var filter:b2FilterData = new b2FilterData();
			var shape:b2Shape = b2PolygonShape.AsBox(B2Transform.pixelsToMeters(w), B2Transform.pixelsToMeters(h));
			var bodyDef:b2BodyDef = new b2BodyDef();
			
			filter.categoryBits = 0x0002;
			filter.maskBits = 0x0002 | 0x0004;
			
			fixtureDef.density = 0;
			fixtureDef.friction = 0;
			fixtureDef.restitution = 1;
			fixtureDef.shape = shape;
			fixtureDef.filter = filter;
			
			bodyDef.type = b2Body.b2_kinematicBody;
			bodyDef.allowSleep = false;
			bodyDef.position.Set(B2Transform.pixelsToMeters(x), B2Transform.pixelsToMeters(y));

			create(bodyDef, fixtureDef, {type:'board', hitted:false});
		}
		
		private function draw(w:Number, h:Number):void 
		{
			var matrix:Matrix = new Matrix();
			
			matrix.createGradientBox(w, h, Math.PI / 2);
			_shape.graphics.clear();
			_shape.graphics.beginFill(0xFFFFFF);
			_shape.graphics.drawRoundRectComplex(-w, -h, w*2 , h*2, 7, 7, 7, 7);
			_shape.graphics.beginGradientFill(GradientType.LINEAR, [0x543425, 0x543425], [0.7, 1], [0x00, 0xFF], matrix);
			_shape.graphics.drawRoundRectComplex(-w, -h, w*2 , h*2, 7, 7, 7, 7);
			_shape.graphics.endFill();
		}
		
		override public function setPosition(x:Number, y:Number):void 
		{
			this.x = x;
			this.y = y;
			super.setPosition(x, y);
		}
		
		override public function setSize(w:Number, h:Number):void
		{
			if ((_width == w) && (_height == h)) return;
			
			_width = (w > MAX_WIDTH) ? MAX_WIDTH : w;
			_height = h;
			draw(_width, _height);
			super.setSize(_width, _height);
		}
		
		public function setSizeSmoothly(w:Number, h:Number):void
		{
			if ((_width == w) && (_height == h)) return;
			
			var sizeObj:Object = {width:_width, height:_height};
			
			_width = (w > MAX_WIDTH) ? MAX_WIDTH : w;
			_height = h;
			super.setSize(_width, _height);
			filters = null;
			_tweens.width = TweenLite.to(sizeObj, 2, {width:_width, heigth:_height, onUpdateParams:[sizeObj], onUpdate:onUpdateSize})
			_tweens.widthGrad = TweenMax.to(this, 0.5, {glowFilter:{color:0x8000FF, blurX:8, blurY:8, strength:1, quality:3, alpha:1}, yoyo:true, repeat:3, overwrite:true});
		}
		
		public function clearEffects():void 
		{
			for (var name:String in _tweens) 
			{
				if (_tweens[name])
				{
					_tweens[name].kill();
				}
			}
			
			filters = null;
		}
		
		public function moveSmothly():void 
		{
			var tail:Shape;
			var v:b2Vec2;
			
			move();
			v = getLinearVelocity();
			
			if (v.x == 0) return;
			
			tail = new Shape();
			tail.graphics.beginFill(0x0080FF, 0.7);
			tail.graphics.drawEllipse(-Math.round(_height*0.8), -Math.round(_height*0.8), Math.round(_height * 1.6), Math.round(_height * 1.6));
			tail.graphics.endFill();
			tail.filters = [new GlowFilter(0xFF8000, 1, 8, 8)];
			tail.x = x + (v.x < 0 ? Math.round(_width - tail.width/2) : Math.round(-_width + tail.width/2));
			tail.y = y;
			if (parent) parent.addChildAt(tail, parent.getChildIndex(this));
			TweenLite.to(tail, 0.3, {alpha:0.1, scaleX:0.5, scaleY:0.5, blurFilter:{blurX:10,blurY:10}, onCompleteParams:[tail], onComplete:removeTail});
		}
		
		private function removeTail(obj:Object):void 
		{
			var tail:Shape = obj as Shape;
			if (tail.parent) tail.parent.removeChild(tail);
		}
		
		public function controlPosition(left:Number, right:Number):void 
		{
			var boardPosition:b2Vec2 = body.GetPosition();
			var w:Number = B2Transform.pixelsToMeters(_width);
				
			if (boardPosition.x - w < left) body.SetPosition(new b2Vec2(left + w, boardPosition.y));
			else if (boardPosition.x + w > right) body.SetPosition(new b2Vec2(right - w, boardPosition.y));
		}
		
		public function setSpeedSmoothly(value:Number):void 
		{
			speed = value;
			filters = null;
			_tweens.speedGrad = TweenMax.to(this, 0.5, {glowFilter:{color:0xFF0000, blurX:8, blurY:8, strength:1, quality:3, alpha:1}, yoyo:true, repeat:3, overwrite:true});
		}
		
		private function onUpdateSize(obj:Object):void 
		{
			draw(obj.width, obj.height);
		}
		
		override public function get width():Number
		{
			return _width;
		}
		
		override public function set width(value:Number):void
		{
			return;
		}
		
		override public function get height():Number
		{
			return _height;
		}
		
		override public function set height(value:Number):void
		{
			return;
		}
		
		public function get speed():Number 
		{
			return _speed;
		}
		
		public function set speed(value:Number):void 
		{
			_speed = value;
		}
		
		public function get direction():int 
		{
			return _direction;
		}
		
		public function set direction(value:int):void 
		{
			_direction = value;
		}
		
		public function get defaultSpeed():Number 
		{
			return _defaultSpeed;
		}
		
		public function set defaultSpeed(value:Number):void 
		{
			_defaultSpeed = value;
		}
	}
	
}