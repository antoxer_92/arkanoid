package model
{
	import Box2D.Collision.Shapes.b2PolygonShape;
	import Box2D.Collision.Shapes.b2Shape;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2BodyDef;
	import Box2D.Dynamics.b2FilterData;
	import Box2D.Dynamics.b2FixtureDef;
	import Box2D.Dynamics.b2World;
	import flash.display.GradientType;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	
	/**
	 * ...
	 * @author Antoxer
	 */
	public class Brick extends Body implements IBrick
	{
		private var _type:int;
		private var _width:Number = 0;
		private var _height:Number = 0;
		private var _health:int;
		
		public function Brick(world:b2World, type:uint, drop:uint, x:Number, y:Number, w:Number, h:Number):void
		{
			super(world);
			_type = type;
			_health = type;
			setSize(w, h);
			setPosition(x, y);
			initBody(drop, x, y, w, h);
		}
		
		private function initBody(drop:uint, x:Number, y:Number, w:Number, h:Number):void 
		{
			var fixtureDef:b2FixtureDef = new b2FixtureDef();
			var filter:b2FilterData = new b2FilterData();
			var shape:b2Shape = b2PolygonShape.AsBox(B2Transform.pixelsToMeters(w), B2Transform.pixelsToMeters(h));
			var bodyDef:b2BodyDef = new b2BodyDef();
			
			filter.categoryBits = 0x0002;
			filter.maskBits = 0x0002;
			
			fixtureDef.density = 1;
			fixtureDef.restitution = 1;
			fixtureDef.shape = shape;
			fixtureDef.filter = filter;

			bodyDef.type = b2Body.b2_staticBody;
			bodyDef.position.Set(B2Transform.pixelsToMeters(x), B2Transform.pixelsToMeters(y));
			
			create(bodyDef, fixtureDef, {type:'brick', hitted:false, drop:drop});
		}
		
		public static function getColor(health:int):uint
		{
			var color:uint;
			
			switch (health) 
			{
				case 1:
					color = 0x0080FF;
				break;
				case 2:
					color = 0x00FF80;
				break;
				case 3:
					color = 0xF8E356;
				break;
				case 4:
					color = 0xFF8000;
				break;
				case 5:
					color = 0xFF4F4F;
				break;
				default:
					color = 0xFFFFFF;
				break;
			}
			
			return color;
		}
		
		private function draw():void 
		{
			var color:uint = getColor(_health);
			var matrix:Matrix = new Matrix();
			
			matrix.createGradientBox(_width, _height, Math.PI / 2);
			graphics.clear();
			graphics.beginFill(0xFFFFFF);
			graphics.drawRoundRectComplex(-_width, -_height, _width*2, _height*2, 7, 7, 7, 7);
			graphics.beginGradientFill(GradientType.LINEAR, [color, color], [0.7, 1], [0x00, 0xFF], matrix);
			graphics.drawRoundRectComplex(-_width, -_height, _width*2, _height*2, 7, 7, 7, 7);
			graphics.endFill();
		}
		
		override public function setPosition(x:Number, y:Number):void 
		{
			this.x = x;
			this.y = y;
			super.setPosition(x, y);
		}
		
		override public function setSize(w:Number, h:Number):void
		{
			if ((_width == w) && (_height == h)) return;
			
			_width = w;
			_height = h;
			draw();
			super.setSize(_width, _height);
		}

		public function get health():int 
		{
			return _health;
		}
		
		public function set health(value:int):void 
		{
			_health = value;
			draw();
		}
		
		override public function get width():Number
		{
			return _width;
		}
		
		override public function set width(value:Number):void
		{
			return;
		}
		
		override public function get height():Number
		{
			return _height;
		}
		
		override public function set height(value:Number):void
		{
			return;
		}
		
		public function get type():int 
		{
			return _type;
		}
		
		public function set type(value:int):void 
		{
			_type = value;
		}
	}
	
}