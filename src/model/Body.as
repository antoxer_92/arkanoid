package model
{
	import Box2D.Collision.Shapes.b2PolygonShape;
	import Box2D.Collision.Shapes.b2Shape;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2BodyDef;
	import Box2D.Dynamics.b2FilterData;
	import Box2D.Dynamics.b2Fixture;
	import Box2D.Dynamics.b2FixtureDef;
	import Box2D.Dynamics.b2World;
	import flash.display.Sprite;
	
	/**
	 * ...
	 * @author Antoxer
	 */
	public class Body extends Sprite implements IBody
	{
		private var _body:b2Body;
		private var _world:b2World;
		
		public function Body(world:b2World):void 
		{
			_world = world
		}
		
		public function move():void
		{
			if (_body == null) return;
			
			x = Math.round(B2Transform.metersToPixels(_body.GetPosition().x));
			y = Math.round(B2Transform.metersToPixels(_body.GetPosition().y));
		}
		
		public function setPosition(x:Number, y:Number):void 
		{
			if (_body == null) return;
			_body.SetPosition(new b2Vec2(B2Transform.pixelsToMeters(x), B2Transform.pixelsToMeters(y)));
		}
		
		public function setSize(w:Number, h:Number):void
		{
			if (_body == null) return;
			
			var currentFixture:b2Fixture = _body.GetFixtureList();
			var fixtureDef:b2FixtureDef = new b2FixtureDef();
			var shape:b2Shape = b2PolygonShape.AsBox(B2Transform.pixelsToMeters(w), B2Transform.pixelsToMeters(h));

			fixtureDef.density = currentFixture.GetDensity();
			fixtureDef.friction = currentFixture.GetFriction();
			fixtureDef.restitution = currentFixture.GetRestitution();
			fixtureDef.shape = shape;
			fixtureDef.filter = currentFixture.GetFilterData().Copy();
			
			_body.DestroyFixture(currentFixture);
			_body.CreateFixture(fixtureDef);
		}
		
		public function setLinearVelocity(vx:Number, vy:Number):void
		{
			if (_body == null) return;
			_body.SetLinearVelocity(new b2Vec2(vx, vy));
		}
		
		public function setAngularVelocity(omega:Number):void
		{
			if (_body == null) return;
			_body.SetAngularVelocity(omega);
		}
		
		public function getLinearVelocity():b2Vec2
		{
			return _body ? _body.GetLinearVelocity() : null;	
		}
		
		public function increaseLinearVelocity(mx:Number, my:Number, bx:Number = 0, by:Number = 0):void
		{
			if (_body == null) return;
			
			var v:b2Vec2 = _body.GetLinearVelocity();
			_body.SetLinearVelocity(new b2Vec2(v.x*mx+bx, v.y*my+by));
		}
		
		public function get body():b2Body 
		{
			return _body;
		}
		
		public function set body(value:b2Body):void 
		{
			_body = value;
		}
		
		protected function create(bodyDef:b2BodyDef, fixtureDef:b2FixtureDef, userData:Object):void
		{
			_body = _world.CreateBody(bodyDef);
			_body.CreateFixture(fixtureDef);
			_body.SetUserData(userData);
		}
		
		public function destroy():void 
		{
			if (parent) parent.removeChild(this);
			if (body) body.GetWorld().DestroyBody(body);
		}
	}
	
}