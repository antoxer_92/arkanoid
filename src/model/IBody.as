package model 
{
	import Box2D.Dynamics.b2Body;
	/**
	 * ...
	 * @author Antoxer
	 */
	public interface IBody 
	{
		function get body():b2Body
		function set body(value:b2Body):void
		
		function setSize(w:Number, h:Number):void
		function destroy():void
	}

}