package controller 
{
	/**
	 * ...
	 * @author Antoxer
	 */
	public class MoveDirection 
	{
		public static const LEFT:int = -1;
		public static const UP:int = -1;
		public static const RIGHT:int = 1;	
		public static const DOWN:int = 1;	
		static public const NONE:int = 0;
	}

}