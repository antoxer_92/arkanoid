package controller
{
	import base.BaseClass;
	import events.GlobalDispatcher;
	import events.GameEvent;
	import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;
	import model.GameState;

	public class Controller extends BaseClass
	{
		private var _boardDirection:int = 0;
		
		public function Controller()
		{
			super();
		}
		
		override protected function onDestroy():void
		{
			
		}
		
		override protected function onCreate():void
		{
			//From View
			GlobalDispatcher.addEventListener(GameEvent.START_GAME, viewHandler);
			GlobalDispatcher.addEventListener(GameEvent.PAUSE_GAME, viewHandler);
			GlobalDispatcher.addEventListener(GameEvent.BACK_TO_MENU, viewHandler);
		
			//From Model
			GlobalDispatcher.addEventListener(GameEvent.STATE_CHANGED, stateChangeHandler);
		}
		
		private function stateChangeHandler(e:GameEvent):void 
		{
			var state:int = e.data.state as uint;
			
			switch (state) 
			{
				//Уровень загрузился
				case GameState.LEVEL_LOADED:
					Facade.stage.addEventListener(KeyboardEvent.KEY_DOWN, keyDownHandler);
					Facade.stage.addEventListener(KeyboardEvent.KEY_UP, keyUpHandler);
				break;
				//Уровень закончен
				case GameState.LEVEL_COMPLETED:
					Facade.stage.removeEventListener(KeyboardEvent.KEY_DOWN, keyDownHandler);
					Facade.stage.removeEventListener(KeyboardEvent.KEY_UP, keyUpHandler);
				break;
			}
		}
		
		private function viewHandler(e:GameEvent):void 
		{
			switch (e.type) 
			{
				case GameEvent.BACK_TO_MENU:
					Facade.model.toInitialState();
				break;
				case GameEvent.START_GAME:
					if ((Facade.model.state == GameState.MAIN_MENU) || (Facade.model.state == GameState.FINISH_MENU) || (Facade.model.state == GameState.WINNER_MENU)) Facade.model.firstLevel();
				break;
				case GameEvent.PAUSE_GAME:
					if (Facade.model.state == GameState.LEVEL_IS_PLAYING) Facade.model.pauseLevel();
					else if (Facade.model.state == GameState.LEVEL_PAUSED) Facade.model.resumeLevel();
				break;
			}
		}

		private function keyUpHandler(e:KeyboardEvent):void 
		{
			if ((e.keyCode == Keyboard.LEFT) && (_boardDirection == MoveDirection.LEFT)) 
			{
				_boardDirection = MoveDirection.NONE;
				Facade.model.setBoardDirection(_boardDirection);
			}
			else if ((e.keyCode == Keyboard.RIGHT) && (_boardDirection == MoveDirection.RIGHT)) 
			{
				_boardDirection = MoveDirection.NONE;
				Facade.model.setBoardDirection(_boardDirection);
			}
		}
		
		private function keyDownHandler(e:KeyboardEvent):void 
		{
			if ((e.keyCode == Keyboard.LEFT) && (_boardDirection != MoveDirection.LEFT))
			{
				_boardDirection = MoveDirection.LEFT;
				Facade.model.setBoardDirection(_boardDirection);
			}
			else if ((e.keyCode == Keyboard.RIGHT) && (_boardDirection != MoveDirection.RIGHT))
			{
				_boardDirection = MoveDirection.RIGHT;
				Facade.model.setBoardDirection(_boardDirection);
			}
			else if (e.keyCode == Keyboard.SPACE)
			{
				if (Facade.model.state == GameState.LEVEL_IS_READY) Facade.model.startLevel();
				else if (Facade.model.state == GameState.LEVEL_IS_PLAYING) Facade.model.pauseLevel();
				else if (Facade.model.state == GameState.LEVEL_PAUSED) Facade.model.resumeLevel();
			}
		}
	}
}