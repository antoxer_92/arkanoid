package view
{
	import flash.display.GradientType;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	
	/**
	 * ...
	 * @author Antoxer
	 */
	public class HudBounds extends Sprite 
	{
		public function HudBounds(gameArea:Rectangle):void
		{
			draw(gameArea);
		}
		
		public function draw(gameArea:Rectangle):void 
		{
			var matrix:Matrix = new Matrix();
			matrix.createGradientBox(1000 - gameArea.right, 800, Math.PI / 2);
			
			graphics.beginGradientFill(GradientType.LINEAR, [0x003A75, 0x221421, 0x002851], [1, 1, 1], [0x00, 0x80, 0xFF], matrix);
			graphics.drawRect(0, 0, gameArea.right, gameArea.top);
			
			graphics.beginGradientFill(GradientType.LINEAR, [0x003A75, 0x221421, 0x002851], [1, 1, 1], [0x00, 0x80, 0xFF], matrix);
			graphics.drawRect(0, gameArea.top, gameArea.left, gameArea.bottom);
		
			graphics.beginGradientFill(GradientType.LINEAR, [0x003A75, 0x221421, 0x002851], [1, 1, 1], [0x00, 0x80, 0xFF], matrix);
			graphics.drawRect(0, gameArea.bottom, gameArea.right, gameArea.top);
			
			graphics.beginGradientFill(GradientType.LINEAR, [0x003A75, 0x221421, 0x002851], [1, 1, 1], [0x00, 0x80, 0xFF], matrix);
			graphics.drawRect(gameArea.right, 0, 1000 - gameArea.right, gameArea.top + gameArea.bottom);
			
			graphics.endFill();
		}
	}
	
}