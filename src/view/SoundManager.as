package view 
{
	import flash.display.Sprite;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	
	/**
	 * ...
	 * @author Antoxer
	 */
	public class SoundManager extends Sprite 
	{
		public static const BALL_OUT:String = 'ballOut';
		public static const MENU:String = 'menu';
		public static const BALL_BOARD:String = 'ballBoard';
		static public const BALL_BRICK:String = "ballBrick";
		static public const BALL_WALL:String = "ballWall";
		static public const LEVEL_COMPLETE:String = "levelComplete";
		static public const BONUS:String = "bonus";

		private var _sounds:Object;

		public function SoundManager() 
		{
			//Создаем экземпляры классов звука
			_sounds = {};
			_sounds[MENU] = {sound:Library.menuMusic, channel:null};
			_sounds[BALL_OUT] = {sound:Library.ballOutSound, channel:null};
			_sounds[BALL_BOARD] = {sound:Library.ballBoardSound, channel:null};
			_sounds[BALL_BRICK] = {sound:Library.ballBrickSound, channel:null};
			_sounds[BALL_WALL] = {sound:Library.ballWallSound, channel:null};
			_sounds[LEVEL_COMPLETE] = {sound:Library.levelCompleteSound, channel:null};
			_sounds[BONUS] = {sound:Library.bonusSound, channel:null};

			super();
		}
		
		public function playSound(name:String, autoRepeat:Boolean = false):void
		{
			stopSound(name);
			_sounds[name].channel = (_sounds[name].sound as Sound).play(0, autoRepeat ? int.MAX_VALUE : 0);
		}
		
		public function stopSound(name:String):void
		{
			if (_sounds[name].channel) 
			{
				(_sounds[name].channel as SoundChannel).stop();
			}
		}
		
		public function stopAll():void
		{
			for (var name:String in _sounds) stopSound(name);
		}
		
	}

}