package view 
{
	import com.greensock.TweenLite;
	import com.greensock.TweenMax;
	import com.greensock.plugins.TweenPlugin;
	import com.greensock.plugins.GlowFilterPlugin; 
	import flash.filters.GlowFilter;
	
	import ru.axr.buttons.LableButton;
	
	/**
	 * ...
	 * @author Antoxer
	 */
	public class TweenButton extends LableButton 
	{
		private var _tween:TweenMax;
		
		public function TweenButton(param0:String="label", param1:String="shape", param2:String="", param3:String="", param4:String="", param5:String="", param6:String="", param7:String="", param8:String="", param9:String="") 
		{
			filters = [new GlowFilter(0xFC6703, 1, 2, 2, 1, 3)];
			TweenPlugin.activate([GlowFilterPlugin]);
			_tween = new TweenMax(this, 1, {glowFilter:{color:0xFC6703, blurX:8, blurY:8, strength:1, quality:3, alpha:1}, onComplete:glowFilterComplete, onReverseComplete:glowFilterReverse});
			super(param0, param1, param2, param3, param4, param5, param6, param7, param8, param9);
		}
		
		private function glowFilterReverse():void 
		{
			_tween.restart();
		}
		
		private function glowFilterComplete():void 
		{
			_tween.reverse();
		}
		
		public function get tween():TweenMax 
		{
			return _tween;
		}
	}

}