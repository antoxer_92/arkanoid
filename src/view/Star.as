package view 
{
	import flash.display.Sprite;
	import com.greensock.TweenLite;
	/**
	 * ...
	 * @author Antoxer
	 */
	public class Star extends Sprite 
	{
		private var _tween:TweenLite;
		
		public function Star() 
		{
			draw();
			super();
		}
		
		public function createTween():void
		{
			_tween = new TweenLite(this, Math.round(200+z)/10, {z:-200, onComplete:completeHandler});
			_tween.pause();
		}
		
		private function completeHandler():void 
		{
			destroy();
		}
		
		public function play():void 
		{
			_tween.play();
		}
		
		public function pause():void 
		{
			_tween.pause();
		}
		
		public function destroy():void 
		{
			parent.removeChild(this);
		}
		
		private function draw():void 
		{
			graphics.beginFill(0xFFFFFF);
			graphics.drawEllipse( -15, -15, 15, 15);
			graphics.endFill();
		}
		
	}

}