package view 
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.GlowFilter;
	import flash.geom.Matrix3D;
	import flash.geom.PerspectiveProjection;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.geom.Utils3D;
	import flash.geom.Vector3D;
	
	import com.greensock.TweenLite;
	
	/**
	 * ...
	 * @author Antoxer
	 */
	public class FlyingStarsManager extends Sprite 
	{
		private var _isPalying:Boolean = false;
		private var _width:Number;
		private var _height:Number;
		private var _stars:Vector.<Star>;
		private var _starsLimit:uint = 300;
		
		public function FlyingStarsManager(width:Number = 200, height:Number = 200) 
		{
			setSize(width, height);
			_stars = new Vector.<Star>;
			super();
		}
		
		public function setSize(width:Number, height:Number):void 
		{
			_width = width;
			_height = height;
		}
		
		private function addedToStageHandler(e:Event):void 
		{
			if (e) removeEventListener(e.type, addedToStageHandler);
		}
		
		public function fill():void
		{
			while (_stars.length < _starsLimit) addStar(0, 1000, 0, 800);
		}
		
		public function play():void
		{
			if (_isPalying) return;
			for (var i:int = 0; i < _stars.length; i++) _stars[i].play();
			addEventListener(Event.ENTER_FRAME, enterFrameHandler);
			_isPalying = true;
		}
		
		public function pause():void
		{
			if (!_isPalying) return;
			for (var i:int = 0; i < _stars.length; i++) _stars[i].pause();
			removeEventListener(Event.ENTER_FRAME, enterFrameHandler);
			_isPalying = false;
		}
		
		public function stop():void
		{
			pause();
			for (var i:int = 0; i < _stars.length; i++) _stars[i].destroy();
			_stars.length = 0;
		}
		
		private function enterFrameHandler(e:Event):void 
		{
			for (var i:int = 0; i < _stars.length; i++)
			{
				if (_stars[i].parent == null) 
				{
					_stars.removeAt(i);
					addStar(0, _width, 0, _height).play();
				}
			}
		}

		private function addStar(x1:Number, x2:Number, y1:Number, y2:Number):Star
		{
			var star:Star = new Star();
			var color:String
			
			star.width = star.height = generateNumber(1, 2);
			star.x = generateNumber(x1, x2);
			star.y = generateNumber(y1, y2);
			star.z = generateNumber(0, 1000);

			color = '0x' + generateUint(0, 255).toString(16) + generateUint(0, 255).toString(16) + '00';
			star.filters = [new GlowFilter(uint(color), 1, 8, 8, 3, 5)];

			star.createTween();
			addChild(star);
			_stars.push(star);
			
			return star;
		}
		
		private static function generateNumber(min:Number, max:Number):Number
		{
			var value:Number = min + Math.random() * (max - min);
			return value;
		}
		
		private static function generateUint(min:uint, max:uint):uint
		{
			var value:uint = Math.round(min + Math.random() * (max - min));
			return value;
		}
	}

}