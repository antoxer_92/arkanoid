package view
{
	import base.BaseClass;
	import com.greensock.TweenLite;
	import com.greensock.TweenMax;
	import com.greensock.easing.Back;
	import com.greensock.easing.Elastic;
	import events.GameEvent;
	import events.GlobalDispatcher;
	import flash.display.DisplayObject;
	import flash.display.GradientType;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.geom.Matrix;
	import flash.geom.Matrix3D;
	import flash.geom.PerspectiveProjection;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.text.AntiAliasType;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import model.Ball;
	import model.Board;
	import model.Brick;
	import model.GameState;
	import model.Model;
	import ru.axr.buttons.events.ButtonEvent;
	import ru.axr.text.AXRTextField;
	import ru.axr.text.AXRTextFieldAlign;
	import ru.axr.text.AXRTextFieldAutoSize;
	import ru.axr.buttons.LabelAlign;
	
	import ru.axr.buttons.LableButton;

	public class View extends BaseClass
	{
		private var _soundManager:SoundManager;
				
		private var _spaceWiew:Sprite = new Sprite();
		private var _background:Sprite = new Sprite();
		private var _space:DisplayObject = Library.space;
		private var _flyingStars:FlyingStarsManager;
		
		private var _menu:Sprite = new Sprite();
		private var _btn_start:TweenButton = Library.tweenButton;
		private var _txt_arkanoid:TextField;
		
		private var _gameScreen:Sprite = new Sprite();
		private var _hudBounds:HudBounds = new HudBounds(Model.gameArea);
		private var _txt_score:TextField;
		private var _txt_level:TextField;
		private var _txt_lives:TextField;
		private var _btn_pause:LableButton = Library.lableSwitch;
		private var _btn_menu:LableButton = Library.lableButton;
		private var _gameChildsArea:Sprite = new Sprite();
		
		private var _endMenu:Sprite = new Sprite();
		private var _txt_totalScore:TextField;
		private var _btn_playAgain:TweenButton = Library.tweenButton;
		
		private var _completeMenu:Sprite = new Sprite();
		private var _txt_totalScore2:TextField;
		private var _txt_author:AXRTextField;
		private var _btn_playAgain2:TweenButton = Library.tweenButton;
		private var _btn_menu2:TweenButton = Library.tweenButton;
		
		private var _hasMenuListeners:Boolean = false;

		private var _score:uint = 0;
		private var _level:uint = 0;
		private var _lives:uint = 0;
		
		public function View()
		{
			super();
		}
		
		override protected function onDestroy():void
		{
			
		}
		
		override protected function onCreate():void
		{
			_soundManager = new SoundManager();
			
			_spaceWiew.scrollRect = new Rectangle(0, 0, 1000, 800);
			Facade.stage.addChild(_spaceWiew);

			var _perspectiveProjection:PerspectiveProjection = new PerspectiveProjection();
			_perspectiveProjection.fieldOfView = 150;
			_perspectiveProjection.projectionCenter = new Point(500, 400);
			_spaceWiew.transform.perspectiveProjection = _perspectiveProjection;
			
			_background.x = 500;
			_background.y = 400;
			//_background.transform.perspectiveProjection = _perspectiveProjection;
			_spaceWiew.addChild(_background);
			//Facade.stage.addChild(_background);

			_space.x = Math.round((-_space.width)/2);
			_space.y = Math.round((-_space.height)/2);
			_background.addChild(_space);
			
			_flyingStars = new FlyingStarsManager(1000, 800);
			_flyingStars.fill();
			_flyingStars.x = -500;
			_flyingStars.y = -400;
			_background.addChild(_flyingStars);

			//_mainView.addChild(_menu);
			Facade.stage.addChild(_menu);
			
			_btn_start.setSize(200, 50);
			_btn_start.label = 'Play';
			_btn_start.x = Math.round((Facade.stage.stageWidth - _btn_start.width) / 2);
			_btn_start.y = Math.round((Facade.stage.stageHeight - _btn_start.height) / 2);
			_btn_start.addEventListener(MouseEvent.CLICK, startHandler);
			_menu.addChild(_btn_start);
			
			_txt_arkanoid = createCustomTextField();
			_txt_arkanoid.defaultTextFormat = new TextFormat(null, 140);
			_txt_arkanoid.autoSize = TextFieldAutoSize.NONE;
			_txt_arkanoid.height = 200;
			_txt_arkanoid.width = 800;
			_txt_arkanoid.wordWrap = false;
			_txt_arkanoid.x = 500 - Math.round(_txt_arkanoid.width / 2);
			_txt_arkanoid.y = 100;
			_txt_arkanoid.text = 'Arkanoid';
			_menu.addChild(_txt_arkanoid);
			
			//_mainView.addChild(_gameScreen);
			Facade.stage.addChild(_gameScreen);
			_gameScreen.cacheAsBitmap = true;
			_gameScreen.addChild(_hudBounds);

			var ofset:Number = Model.gameArea.right + Model.gameArea.left;
			_txt_level = createCustomTextField();
			_txt_level.x = ofset + Math.round((Facade.stage.stageWidth - ofset - _txt_level.width) / 2);
			_txt_level.y = 10;
			_gameScreen.addChild(_txt_level);
			
			_txt_score = createCustomTextField();
			_txt_score.x = ofset + Math.round((Facade.stage.stageWidth - ofset - _txt_score.width) / 2);
			_txt_score.y = _txt_level.y + _txt_level.height + 120;
			_gameScreen.addChild(_txt_score);
			
			_txt_lives = createCustomTextField();
			_txt_lives.x = ofset + Math.round((Facade.stage.stageWidth - ofset - _txt_lives.width) / 2);
			_txt_lives.y = _txt_score.y + _txt_score.height + 120;
			_gameScreen.addChild(_txt_lives);
			
			//_btn_pause.setSize(150, 40);
			_btn_pause.toggle = true;
			_btn_pause.x = ofset + Math.round((Facade.stage.stageWidth - ofset - _btn_pause.width) / 2);
			_btn_pause.y = Facade.stage.stageHeight - _btn_pause.height - 30;
			_btn_pause.addEventListener(ButtonEvent.SWITCH, pauseHandler);
			_gameScreen.addChild(_btn_pause);
			
			//_btn_menu.setSize(150, 40);
			_btn_menu.label = 'Menu';
			_btn_menu.x = ofset + Math.round((Facade.stage.stageWidth - ofset - _btn_menu.width) / 2);
			_btn_menu.y = _btn_pause.y - 10 - _btn_menu.height;
			_btn_menu.addEventListener(MouseEvent.CLICK, menuClickHandler);
			_gameScreen.addChild(_btn_menu);

			_gameScreen.addChild(_gameChildsArea);
			
			//_mainView.addChild(_endMenu);
			Facade.stage.addChild(_endMenu);
			
			_txt_totalScore = createCustomTextField();
			_txt_totalScore.autoSize = TextFieldAutoSize.NONE;
			_txt_totalScore.height = 70;
			_txt_totalScore.width = 500;
			_txt_totalScore.wordWrap = false;
			_txt_totalScore.x = Math.round((Facade.stage.stageWidth - _txt_totalScore.width) / 2);
			_txt_totalScore.y = 300;
			_txt_totalScore.text = 'Total score: 0';
			_endMenu.addChild(_txt_totalScore);
			
			_btn_playAgain.setSize(200, 50);
			_btn_playAgain.label = 'Play';
			_btn_playAgain.x = Math.round((Facade.stage.stageWidth - _btn_playAgain.width) / 2);
			_btn_playAgain.y = Math.round((Facade.stage.stageHeight - _btn_start.height) / 2);
			_btn_playAgain.addEventListener(MouseEvent.CLICK, startHandler);
			_endMenu.addChild(_btn_playAgain);
			
			Facade.stage.addChild(_completeMenu);
			
			_txt_totalScore2 = createCustomTextField();
			_txt_totalScore2.autoSize = TextFieldAutoSize.NONE;
			_txt_totalScore2.height = 200;
			_txt_totalScore2.width = 980;
			_txt_totalScore2.defaultTextFormat = new TextFormat(null, null, null, null, null, null, null, null, null, null, null, null, -10);
			_txt_totalScore2.wordWrap = false;
			_txt_totalScore2.x = Math.round((Facade.stage.stageWidth - _txt_totalScore2.width) / 2);
			_txt_totalScore2.y = 200;
			_txt_totalScore2.text = 'Congratulations, you have completed the game\nTotal score: ';
			_completeMenu.addChild(_txt_totalScore2);
			
			_btn_playAgain2.setSize(200, 50);
			_btn_playAgain2.label = 'Play';
			_btn_playAgain2.x = Math.round((Facade.stage.stageWidth - _btn_playAgain2.width) / 2);
			_btn_playAgain2.y = Math.round((Facade.stage.stageHeight - _btn_playAgain2.height) / 2);
			_btn_playAgain2.addEventListener(MouseEvent.CLICK, startHandler);
			_completeMenu.addChild(_btn_playAgain2);
			
			_btn_menu2.setSize(200, 50);
			_btn_menu2.label = 'Menu';
			_btn_menu2.x = Math.round((Facade.stage.stageWidth - _btn_menu2.width) / 2);
			_btn_menu2.y = _btn_playAgain2.y + _btn_playAgain2.height + 15;
			_btn_menu2.addEventListener(MouseEvent.CLICK, menuClickHandler);
			_completeMenu.addChild(_btn_menu2);
			
			smile;
			_txt_author = new AXRTextField();
			_txt_author.fontName = 'Segoe Print';
			_txt_author.fontSize = 20;
			_txt_author.textColor = 0xFFFFFF;
			_txt_author.leading = 4;
			_txt_author.autoSizeH = AXRTextFieldAutoSize.LEFT;
			_txt_author.autoSizeV = AXRTextFieldAutoSize.TOP;
			_txt_author.align = AXRTextFieldAlign.CENTER;
			_txt_author.htmlText = 'Designed by <u><a href="https://hh.ru/resume/01bcf26fff03ac2c500039ed1f363775445361">Antoxer<sub>inc</sub></u><img src="smile" width="32" height="32" align="center"></a>';
			_txt_author.x = Math.round((Facade.stage.stageWidth - _txt_author.width) / 2);
			_txt_author.y = 780 - _txt_author.height;
			_completeMenu.addChild(_txt_author);
			
			GlobalDispatcher.addEventListener(GameEvent.STATE_CHANGED, stateChangeHandler);
			GlobalDispatcher.addEventListener(GameEvent.PLAY_SOUND, playSoundHandler);
		}
		
		private function createCustomTextField():TextField
		{
			var textFormat:TextFormat = new TextFormat('Segoe Print', 40, 0xFFFFFF);
			var tf:TextField = new TextField()
			
			textFormat.align = TextFormatAlign.CENTER;
			textFormat.leading = -30;
			tf.defaultTextFormat = textFormat;
			tf.selectable = false;
			tf.autoSize = TextFieldAutoSize.CENTER;
			tf.antiAliasType = AntiAliasType.ADVANCED;

			return tf;
		}
		
		private function playSoundHandler(e:GameEvent):void 
		{
			playSound(e.data.name);
		}
		
		private function playSound(name:String, autoRepeat:Boolean = false):void
		{
			_soundManager.playSound(name, autoRepeat);
		}
		
		private function stopSound(name:String):void
		{
			_soundManager.stopSound(name);
		}

		//Обработчик события изменения состояния модели
		//БЕЗ ИСПОЛЬЗОВАНИЯ "ШАБЛОНА СОСТОЯНИЯ"
		private function stateChangeHandler(e:GameEvent):void 
		{
			//Перейдем на нужную нам страницу
			var state:int = e.data.state as uint;
			
			switch (state) 
			{
				case GameState.MAIN_MENU:
					hideGameScreen();
					hideEndMenu();
					hideCompleteMenu();
					showMainMenu();
				break;
				case GameState.LEVEL_IS_LOADING:
					hideMainMenu();
					hideGameScreen();
					hideEndMenu();
					hideCompleteMenu();
				break;
				case GameState.LEVEL_LOADED:
					addBricks();
				break;
				case GameState.LEVEL_IS_READY:
					hideMainMenu();
					hideEndMenu();
					showGameScreen();
					_btn_pause.enabled = false;
					_btn_pause.selected = false;
				break;
				case GameState.LEVEL_IS_PLAYING:
					_btn_pause.enabled = true;
					_btn_pause.selected = false;
				break;
				case GameState.LEVEL_PAUSED:
					_btn_pause.selected = true;
				break;
				case GameState.FINISH_MENU:
					hideMainMenu();
					hideGameScreen();
					showEndMenu();
				break;
				case GameState.WINNER_MENU:
					hideMainMenu();
					hideEndMenu();
					hideGameScreen();
					showCompleteMenu();
				break;
			}
		}
		
		public function addGameChild(child:DisplayObject):void 
		{
			_gameChildsArea.addChild(child);
		}
		
		public function showVanishText(text:String, x:Number, y:Number):void 
		{
			var vanishText:VanishingText = new VanishingText();

			vanishText.vanishStep = 0.03;
			vanishText.text = text;
			vanishText.x = Math.round(x - vanishText.width/2);
			vanishText.y = Math.round(y - vanishText.height / 2);
			_gameScreen.addChild(vanishText);
		}
		
		private function addBricks():void 
		{
			var bricks:Vector.<Brick> = Facade.model.bricks;
			for (var i:int = 0; i < bricks.length; i++) addGameChild(bricks[i]);
		}
		
		private function showMainMenu():void
		{
			_menu.visible = true;
			playSound(SoundManager.MENU, true);
			_flyingStars.play();
			_btn_start.z = 1000;
			_txt_arkanoid.z = 500;
			_txt_arkanoid.y = -200;
			TweenLite.to(_txt_arkanoid, 2, {ease: Elastic.easeOut.config(0.1, 0.3), z:0, y:100});
			TweenLite.to(_btn_start, 2, {ease: Elastic.easeOut.config(0.1, 0.3), z:0});
			addMenuListeners(); 
		}
		
		private function hideMainMenu():void
		{
			_menu.visible = false;
			stopSound(SoundManager.MENU);
			removeMenuListeners();
			_background.rotationY = 0;
			_background.rotationX = 0;
		}
		
		private function showEndMenu():void
		{
			_endMenu.visible = true;
			_txt_totalScore.text = 'Total score: ' + String(_score);
			playSound(SoundManager.MENU, true);
			_flyingStars.play();
			_txt_totalScore.z = 500;
			_txt_totalScore.y = 0;
			_btn_playAgain.z = 1000;
			TweenLite.to(_txt_totalScore, 2, {ease: Elastic.easeOut.config(0.1, 0.3), z:0, y:300});
			TweenLite.to(_btn_playAgain, 2, {ease: Elastic.easeOut.config(0.1, 0.3), z:0, y:Math.round((Facade.stage.stageHeight - _btn_start.height) / 2)});
			addMenuListeners(); 
		}
		
		private function hideEndMenu():void
		{
			_endMenu.visible = false;
			stopSound(SoundManager.MENU);
			removeMenuListeners();
			_background.rotationY = 0;
			_background.rotationX = 0;
		}
		
		private function showCompleteMenu():void
		{
			_completeMenu.visible = true;
			_txt_totalScore2.text = 'Congratulations, you have completed the game\nTotal score: ' + String(_score);
			playSound(SoundManager.MENU, true);
			_flyingStars.play();
			_txt_totalScore2.z = 500;
			_txt_totalScore2.y = 0;
			_btn_playAgain2.z = 1000;
			_btn_menu2.z = 1000;
			TweenLite.to(_txt_totalScore2, 2, {ease: Elastic.easeOut.config(0.1, 0.3), z:0, y:200});
			TweenLite.to(_btn_playAgain2, 2, {ease: Elastic.easeOut.config(0.1, 0.3), z:0, y:350});
			TweenLite.to(_btn_menu2, 2, {ease: Elastic.easeOut.config(0.1, 0.3), z:0, y:420});
			addMenuListeners(); 
		}
		
		private function hideCompleteMenu():void
		{
			_completeMenu.visible = false;
			stopSound(SoundManager.MENU);
			removeMenuListeners();
			_background.rotationY = 0;
			_background.rotationX = 0;
		}
		
		private function showGameScreen():void
		{
			_gameScreen.visible = true;
		}
		
		private function hideGameScreen():void
		{
			_gameScreen.visible = false;
		}
		
		private function addMenuListeners():void 
		{
			if (_hasMenuListeners) return;
			Facade.stage.addEventListener(MouseEvent.MOUSE_MOVE, mouseMoveHandler);
			_hasMenuListeners = true;
		}
		
		private function removeMenuListeners():void 
		{
			if (!_hasMenuListeners) return;
			Facade.stage.removeEventListener(MouseEvent.MOUSE_MOVE, mouseMoveHandler);
			_hasMenuListeners = false;
		}
		
		private function mouseMoveHandler(e:MouseEvent):void 
		{
			var angleX:Number = Math.round((500 - e.stageX)/10)*Math.PI/180;
			var angleY:Number = -Math.round((400 - e.stageY)/10)*Math.PI/180;

			TweenLite.to(_background, 4, {ease: Back.easeOut.config(4), rotationY:angleX, rotationX:angleY});
		}

		private function pauseHandler(e:ButtonEvent):void 
		{
			e.preventDefault();
			GlobalDispatcher.dispatchEvent(new GameEvent(GameEvent.PAUSE_GAME));
		}
		
		private function startHandler(e:MouseEvent):void 
		{
			GlobalDispatcher.dispatchEvent(new GameEvent(GameEvent.START_GAME));
		}
		
		private function menuClickHandler(e:MouseEvent):void 
		{
			GlobalDispatcher.dispatchEvent(new GameEvent(GameEvent.BACK_TO_MENU));
		}
		
		private function get stageWidth():Number
		{
			return Facade.stage.stageWidth;
		}
		
		private function get stageHeight():Number
		{
			return Facade.stage.stageHeight;
		}
		
		public function get score():uint 
		{
			return _score;
		}
		
		public function set score(value:uint):void 
		{
			_score = value;
			_txt_score.text = 'Score\n' + String(_score);
			_txt_score.filters = null;
			TweenMax.to(_txt_score, 0.3, {glowFilter:{color:0xFF2F2F, blurX:16, blurY:16, strength:2, quality:3, alpha:1}, yoyo:true, repeat:3, overwrite:true});
		}
		
		public function get level():uint 
		{
			return _level;
		}
		
		public function set level(value:uint):void 
		{
			_level = value;
			_txt_level.text = 'Level\n' + String(_level);
			_txt_level.filters = null;
			TweenMax.to(_txt_level, 0.3, {glowFilter:{color:0x00FF00, blurX:16, blurY:16, strength:2, quality:3, alpha:1}, yoyo:true, repeat:3, overwrite:true});
		}
		
		public function get lives():uint 
		{
			return _lives;
		}
		
		public function set lives(value:uint):void 
		{
			_lives = value;
			_txt_lives.text = 'Lives\n' + String(_lives);
			_txt_lives.filters = null;
			TweenMax.to(_txt_lives, 0.3, {glowFilter:{color:0x0080FF, blurX:16, blurY:16, strength:2, quality:3, alpha:1}, yoyo:true, repeat:3, overwrite:true});
		}
	}
}