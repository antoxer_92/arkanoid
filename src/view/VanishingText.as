﻿package view
{
    import flash.display.Sprite;
	import flash.events.Event;
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	import ru.axr.text.AXRTextField;
	import ru.axr.text.AXRTextFieldAlign;
	import ru.axr.text.AXRTextFieldAutoSize;
	
	public class VanishingText extends Sprite
	{
		private var textField:AXRTextField = new AXRTextField();
		private var tmr_vanishing:Timer = new Timer(30, 100);
		private var tmr_delay:Timer = new Timer(0,1);
		private var _width:Number = 100;
		private var _vanishStep:Number = 0.01;
		private var _autoRemove:Boolean = true;
		private var _wait:Boolean = false;
		private var _vanish:Boolean = false;
		
		public function VanishingText()
		{
			textField.height = 25;
			textField.width = _width;
			addChild(textField);
			textField.fontName = 'Segoe Print';
			textField.fontSize = 30;
			textField.textColor = 0xFFFFFF;
			textField.leading = 4;
			textField.autoSizeH = AXRTextFieldAutoSize.LEFT;
			textField.autoSizeV = AXRTextFieldAutoSize.TOP;
			textField.align = AXRTextFieldAlign.CENTER;
			
			/*if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);*/
		}
		
		/*private function init(e:Event = null):void 
		{
			if (e) removeEventListener(e.type, init);
			
			addEventListener(Event.REMOVED_FROM_STAGE, fin);
		}
		
		private function fin(e:Event):void 
		{
			removeEventListener(e.type, fin);
		}*/

		public function destroy():void
		{
			stopWait();
			stopVanishing();
			removeChild(textField);
			if (parent) parent.removeChild(this);
		}
		
		override public function set width(n:Number):void
		{
			if (_width != n)
			{
				_width = n;
				textField.width =_width;
			}
		}
		
		override public function get width():Number
		{
			return _width;
		}
		
		public function set vanishStep(n:Number):void
		{
			if (_vanishStep == n) return
			
			_vanishStep = n;
		}
		
		public function get vanishStep():Number
		{
			return _vanishStep;
		}
		
		public function set delay(n:Number):void
		{
			if (tmr_delay.delay == n) return;
			tmr_delay.delay = n;
		}
		
		public function get delay():Number
		{
			return tmr_delay.delay;
		}
		
		public function set color(u:uint):void
		{
			if (textField.textColor != u)
			{
				textField.textColor = u;
			}
		}
		
		public function get color():uint
		{
			return textField.textColor;
		}
		
		public function set fontName(s:String):void
		{
			if (textField.fontName != s)
			{
				textField.fontName = s;
			}
		}
		
		public function get fontName():String
		{
			return textField.fontName;
		}
		
		public function set fontSize(u:uint):void
		{
			if (textField.fontSize != u)
			{
				textField.fontSize = u;
			}
		}
		
		public function get fontSize():uint
		{
			return textField.fontSize;
		}
		
		public function set text(s:String):void
		{
			textField.htmlText = s;
			textField.visible = true;
			textField.alpha = 1;
			tmr_vanishing.reset();
			tmr_delay.reset();
			
			if (delay == 0) startVanishing();
			else startWait();
		}
		
		private function startWait():void 
		{
			if (_wait) return;
			_wait = true;
			tmr_delay.addEventListener(TimerEvent.TIMER_COMPLETE, stopWait);
			tmr_delay.start();
		}
		
		private function stopWait(e:TimerEvent = null):void 
		{
			if (!_wait) return;
			_wait = false;
			startVanishing();
		}
		
		private function startVanishing():void 
		{
			if (_vanish) return;
			_vanish = true;
			addEventListener(Event.ENTER_FRAME, onVanishing);
		}
		
		private function stopVanishing():void 
		{
			if (!_vanish) return;
			_vanish = false;
			removeEventListener(Event.ENTER_FRAME, onVanishing);
			
			if (_autoRemove) destroy();
			else
			{
				textField.htmlText = '';
				textField.alpha = 1;
				textField.visible = false;
			}
		}
		
		private function onVanishing(e:Event):void 
		{
			textField.alpha -= _vanishStep;
			
			if (textField.alpha > 0) return;

			stopVanishing();
		}
		
		public function get text():String
		{
			return textField.htmlText;
		}
		
		public function get autoRemove():Boolean 
		{
			return _autoRemove;
		}
		
		public function set autoRemove(value:Boolean):void 
		{
			_autoRemove = value;
		}
	}
}
