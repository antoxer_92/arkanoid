package
{
	import flash.display.Sprite;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import controller.Controller;
	import model.Model;
	import view.View;
	import events.*
	
	[SWF(width=1000, height=800, frameRate=60)]
	
	public class Main extends Sprite
	{
		public function Main()
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void
		{
			if (e) removeEventListener(e.type, init);
			
			stage.scaleMode = StageScaleMode.NO_SCALE;
			
			Facade.stage = stage;
			Facade.view = new View();
			Facade.model = new Model();
			Facade.controller = new Controller();
		}
	}
}