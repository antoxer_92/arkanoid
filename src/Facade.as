package
{
	import controller.Controller;
	import flash.display.Stage;
	import model.Model;
	import view.View;

	public class Facade
	{
		public static var stage:Stage;
		public static var model:Model;
		public static var controller:Controller;
		public static var view:View;
		
	}
}