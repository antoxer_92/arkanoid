package events
{
	import flash.events.EventDispatcher;

	public class GlobalDispatcher
	{
		private static var _dispatcher:EventDispatcher = new EventDispatcher();
		
		public static function dispatchEvent(e:GameEvent):void
		{
			_dispatcher.dispatchEvent(e);
		}
		
		public static function addEventListener(pType:String, pListener:Function):void
		{
			_dispatcher.addEventListener(pType, pListener);
		}
	}
	
	
}