package events
{
	import flash.events.Event;
	
	public class GameEvent extends Event
	{
		//From View
		static public const START_GAME:String = "startGame";
		static public const PAUSE_GAME:String = "pauseGame";
		
		//From Model
		static public const STATE_CHANGED:String = "stateChanged";
		static public const PLAY_SOUND:String = "playSound";
		static public const BACK_TO_MENU:String = "backToMenu";

		private var _data:Object;
		
		public function GameEvent(type:String, data:Object = null, bubbles:Boolean = false, cancelable:Boolean = false)
		{
			_data = data;
			super(type, bubbles, cancelable);
		}
		
		public function get data():Object
		{
			return _data;
		}
	}
}