package 
{
	import flash.display.DisplayObject;
	import flash.filters.BlurFilter;
	import flash.filters.GlowFilter;
	import flash.media.Sound;
	import mx.core.SoundAsset;
	import ru.axr.buttons.ButtonPatterns;
	import ru.axr.buttons.LabelPatterns;
	import ru.axr.buttons.LableButton;
	import view.TweenButton;
	
	/**
	 * ...
	 * @author Antoxer
	 */
	public class Library 
	{
		[Embed(source = "../lib/background4.jpg")]
		private static var spaceClass:Class;
		
		[Embed(source = "../lib/moon5.png")]
		private static var ballClass:Class;
		
		[Embed(source = "../lib/drop1.png")]
		private static var drop1Class:Class
		
		[Embed(source = "../lib/drop2.png")]
		private static var drop2Class:Class
		
		[Embed(source = "../lib/drop3.png")]
		private static var drop3Class:Class
		
		[Embed(source = "../lib/drop4.png")]
		private static var drop4Class:Class
		
		[Embed(source = "../lib/drop5.png")]
		private static var drop5Class:Class
		
		[Embed(source = "../lib/ball_out.mp3")]
		private static var ballOutSoundClass:Class;
		
		[Embed(source = "../lib/menu_music.mp3")]
		private static var menuMusicClass:Class;

		[Embed(source = "../lib/ball_board.mp3")]
		private static var ballBoardSoundClass:Class;
		
		[Embed(source = "../lib/ball_brick.mp3")]
		private static var ballBrickSoundClass:Class
		
		[Embed(source = "../lib/ball_wall.mp3")]
		private static var ballWallSoundClass:Class
		
		[Embed(source = "../lib/level_complete.mp3")]
		private static var levelCompleteSoundClass:Class
		
		[Embed(source = "../lib/bonus.mp3")]
		private static var bonusSoundClass:Class
		
		public static function get space():DisplayObject
		{
			return (new spaceClass() as DisplayObject);
		}
		
		public static function get ball():DisplayObject
		{
			return (new ballClass() as DisplayObject);
		}
		
		public static function get drop1():DisplayObject
		{
			var dO:DisplayObject = new drop1Class() as DisplayObject
			dO.filters = [new GlowFilter(0x8000FF, 0.7, 8, 8)];
			return dO;
		}
		
		public static function get drop2():DisplayObject
		{
			var dO:DisplayObject = new drop2Class() as DisplayObject
			dO.filters = [new GlowFilter(0xFF0000, 0.7, 8, 8)];
			return dO;
		}
		
		public static function get drop3():DisplayObject
		{
			var dO:DisplayObject = new drop3Class() as DisplayObject
			dO.filters = [new GlowFilter(0x0080FF, 0.7, 8, 8)];
			return dO;
		}
		
		public static function get drop4():DisplayObject
		{
			var dO:DisplayObject = new drop4Class() as DisplayObject
			dO.filters = [new GlowFilter(0xFF8000, 0.7, 8, 8)];
			return dO;
		}
		
		public static function get drop5():DisplayObject
		{
			var dO:DisplayObject = new drop5Class() as DisplayObject
			dO.filters = [new GlowFilter(0x00FF00, 0.7, 8, 8)];
			return dO;
		}

		public static function get menuMusic():Sound
		{
			return new menuMusicClass() as Sound;
		}
		
		public static function get ballOutSound():Sound
		{
			return new ballOutSoundClass() as Sound;
		}

		public static function get ballBoardSound():Sound
		{
			return new ballBoardSoundClass() as Sound;
		}
		
		public static function get ballBrickSound():Sound 
		{
			return new ballBrickSoundClass() as Sound;
		}
		
		public static function get ballWallSound():Sound 
		{
			return new ballWallSoundClass() as Sound;
		}
		
		public static function get levelCompleteSound():Sound 
		{
			return new levelCompleteSoundClass() as Sound;
		}
		
		public static function get bonusSound():Sound 
		{
			return new bonusSoundClass() as Sound;
		}
		
		public static function get lableButton():LableButton
		{
			var btn:LableButton = new LableButton();
		
			var pattern:Object = ButtonPatterns.standardButton;
			pattern.out.fill.colors = [0xFF9800, 0xA6751A];
			pattern.over.fill.colors = [0xFF9800, 0xA6751A];
			pattern.down.fill.colors = [0xFF9800, 0xA6751A];
			pattern.disabled.fill.colors = [0xFF9800, 0xA6751A];
			pattern.selectedOut.fill.colors = [0xFF9800, 0xA6751A];
			pattern.selectedOver.fill.colors = [0xFF9800, 0xA6751A];
			pattern.selectedDown.fill.colors = [0xFF9800, 0xA6751A];
			pattern.selectedDisabled.fill.colors = [0xFF9800, 0xA6751A];
			
			
			var lablePattern:Object = LabelPatterns.standardLabel;
			lablePattern.out.textFormat.font = 'Segoe Print';
			lablePattern.over.textFormat.font = 'Segoe Print';
			lablePattern.down.textFormat.font = 'Segoe Print';
			lablePattern.disabled.textFormat.font = 'Segoe Print';
			lablePattern.out.textFormat.color = 0xFFFFFF;
			lablePattern.over.textFormat.color = 0xFFFFFF;
			lablePattern.down.textFormat.color = 0xFFFFFF;
			lablePattern.disabled.textFormat.color = 0xFFFFFF;

			btn.graphicParams = pattern;
			btn.labelParams = lablePattern;
			
			return btn;
		}
		
		public static function get tweenButton():TweenButton
		{
			var btn:TweenButton = new TweenButton();
			
			var pattern:Object = ButtonPatterns.standardButton;
			pattern.out.fill.colors = [0xFF9800, 0xA6751A];
			pattern.over.fill.colors = [0xFF9800, 0xA6751A];
			pattern.down.fill.colors = [0xFF9800, 0xA6751A];
			pattern.disabled.fill.colors = [0xFF9800, 0xA6751A];
			pattern.selectedOut.fill.colors = [0xFF9800, 0xA6751A];
			pattern.selectedOver.fill.colors = [0xFF9800, 0xA6751A];
			pattern.selectedDown.fill.colors = [0xFF9800, 0xA6751A];
			pattern.selectedDisabled.fill.colors = [0xFF9800, 0xA6751A];
			
			
			var lablePattern:Object = LabelPatterns.standardLabel;
			lablePattern.out.textFormat.font = 'Segoe Print';
			lablePattern.over.textFormat.font = 'Segoe Print';
			lablePattern.down.textFormat.font = 'Segoe Print';
			lablePattern.disabled.textFormat.font = 'Segoe Print';
			lablePattern.out.textFormat.size = 35;
			lablePattern.over.textFormat.size = 35;
			lablePattern.down.textFormat.size = 35;
			lablePattern.disabled.textFormat.size = 35;
			lablePattern.out.textFormat.color = 0xFFFFFF;
			lablePattern.over.textFormat.color = 0xFFFFFF;
			lablePattern.down.textFormat.color = 0xFFFFFF;
			lablePattern.disabled.textFormat.color = 0xFFFFFF;

			btn.graphicParams = pattern;
			btn.labelParams = lablePattern;
			
			return btn;
		}
		
		public static function get lableSwitch():LableButton
		{
			var btn:LableButton = lableButton;
			var pattern:Object = btn.graphicParams;
			btn.label = 'Pause';
			
			var lablePattern:Object = lableButton.labelParams;
			lablePattern.selectedOut.textFormat.font = 'Segoe Print';
			lablePattern.selectedOver.textFormat.font = 'Segoe Print';
			lablePattern.selectedDown.textFormat.font = 'Segoe Print';
			lablePattern.selectedDisabled.textFormat.font = 'Segoe Print';
			lablePattern.selectedOut.textFormat.color = 0xFFFFFF;
			lablePattern.selectedOver.textFormat.color = 0xFFFFFF;
			lablePattern.selectedDown.textFormat.color = 0xFFFFFF;
			lablePattern.selectedDisabled.textFormat.color = 0xFFFFFF;
			
			lablePattern.out.label = 'Pause';
			lablePattern.over.label = 'Pause';
			lablePattern.down.label = 'Pause';
			lablePattern.disabled.label = 'Pause';
			
			lablePattern.selectedOut.label = 'Resume';
			lablePattern.selectedOver.label = 'Resume';
			lablePattern.selectedDown.label = 'Resume';
			lablePattern.selectedDisabled.label = 'Resume';
			
			(pattern.selectedOut.filters as Array).unshift(new BlurFilter(2, 2));
			(pattern.selectedOver.filters as Array).unshift(new BlurFilter(2, 2));
			(pattern.selectedDown.filters as Array).unshift(new BlurFilter(2, 2));
			(pattern.selectedDisabled.filters as Array).unshift(new BlurFilter(2, 2));
			(pattern.selectedOut.filters as Array).unshift(new BlurFilter(2, 2));
			(pattern.selectedOver.filters as Array).unshift(new BlurFilter(2, 2));
			(pattern.selectedDown.filters as Array).unshift(new BlurFilter(2, 2));
			(pattern.selectedDisabled.filters as Array).unshift(new BlurFilter(2, 2));
			
			btn.labelParams = lablePattern;
			btn.graphicParams = pattern;
			btn.draw();
			
			return btn;
		}
	}
}